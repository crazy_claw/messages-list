import { Module } from 'vuex';
import get from 'lodash/get';
import addToDate from 'date-fns/add';
import formatDate from 'date-fns/format';
import formatISO from 'date-fns/formatISO';
import { IRootState } from '@/store';
import MessageEntity from '@/entities/message-entity';
import { getMessages, getMessageChangelog, TMesssages, TMessageChangelog } from '@/services/messages-service';
import saveAsCsv from '@/utils/save-as-csv';

export const MESSAGES_STORE_NAME = 'MESSAGES';

const tableHeadersSelectOptions = [
	{
		text: 'Id сообщения',
		value: 'id',
	},
	{
		text: 'Контакт',
		value: 'contacts',
	},
	{
		text: 'Заказ',
		value: 'orderId',
	},
	{
		text: 'Доставка',
		value: 'deliveryId',
	},
	{
		text: 'Событие',
		value: 'event',
	},
	{
		text: 'Канал',
		value: 'channel',
	},
	{
		text: 'Партнер',
		value: 'partnerId',
	},
	{
		text: 'Статус',
		value: 'status',
	},
] as const;

export const defaultTableHeaders = [
	{
		text: 'Дата создания сообщения',
		value: 'creationDateFormatted',
	},
	...tableHeadersSelectOptions,
] as const;

const currentDate = new Date();

export default function getMessagesStore(): Module<IState, IRootState> {
	return {
		namespaced: true,
		state: (): IState => ({
			messages: [] as TMesssages,
			changelog: [],
			errorMessage: '',
			isRequestInprogress: false,
			filterDateFrom: formatISO(addToDate(currentDate, { years: -1 })),
			filterDateTo: formatISO(addToDate(currentDate, { years: 1 })),
			allowedFields: defaultTableHeaders.map(i => i.value),
		}),

		getters: {
			messages({ messages }): TGetterMessages {
				return messages.map(m => new MessageEntity(m));
			},
			changelog({ changelog }) {
				return changelog.map(item => ({
					createdAt: formatDate(new Date(get(item, 'createdAt', '')), 'dd.MM.yyyy kk:mm'),
					statusCode: get(item, 'changes.new.statusCode', ''),
					initiatedBy: get(item, 'initiatedBy', ''),
				}));
			},
			errorMessage({ errorMessage }): string {
				return errorMessage;
			},
			isRequestInprogress({ isRequestInprogress }): boolean {
				return isRequestInprogress;
			},
			filterDateFrom({ filterDateFrom }): string {
				return filterDateFrom;
			},
			filterDateTo({ filterDateTo }): string {
				return filterDateTo;
			},
			allowedTableHeaders({ allowedFields }) {
				return defaultTableHeaders.filter(header => allowedFields.includes(header.value));
			},
		},

		mutations: {
			messages(state, value: TMesssages) {
				state.messages = value;
			},
			changelog(state, value: TMessageChangelog) {
				state.changelog = value;
			},
			errorMessage(state, value: string) {
				state.errorMessage = value;
			},
			isRequestInprogress(state, value: boolean) {
				state.isRequestInprogress = value;
			},
			filterDateFrom(state, value: string) {
				state.filterDateFrom = value;
			},
			filterDateTo(state, value: string) {
				state.filterDateTo = value;
			},
			allowedFields(state, value: string[]) {
				state.allowedFields = value;
			},
		},

		actions: {
			/** Получить историю сообщений */
			async fetchMessages(
				{ commit, state: { filterDateFrom, filterDateTo } },
				customFilters: TCustomFilters = {},
			) {
				commit('errorMessage', '');
				commit('isRequestInprogress', true);
				try {
					const messages = await getMessages({
						...customFilters,
						createdAfter: filterDateFrom,
						createdBefore: filterDateTo,
						limit: 300,
					});
					commit('messages', messages);
				} catch (error) {
					commit('errorMessage', 'Не удалось получить список сообщений');
					throw new Error(error);
				} finally {
					commit('isRequestInprogress', false);
				}
			},

			/** Получить changelog для одного сообщения */
			async fetchMessageChangelog({ commit }, messageId) {
				commit('errorMessage', '');
				commit('isRequestInprogress', true);
				try {
					const changelog = await getMessageChangelog({ messageId });
					commit('changelog', changelog);
				} catch (error) {
					commit('errorMessage', `Не удалось получить changelog для сообщения ${messageId}`);
				} finally {
					commit('isRequestInprogress', false);
				}
			},

			/** Выбрать дату начала периода для получения истории сообщений */
			async selectDateFrom({ commit, dispatch }, dateFrom: string) {
				commit('filterDateFrom', dateFrom);
				await dispatch('fetchMessages');
			},

			/** Выбрать дату конца периода для получения истории сообщений */
			async selectDateTo({ commit, dispatch }, dateTo: string) {
				commit('filterDateTo', dateTo);
				await dispatch('fetchMessages');
			},

			/** Изменить кастомный фильтр истории сообщений */
			async changeCustomFilter({ dispatch }, filters: TCustomFilter[]) {
				const customFilters: TCustomFilters = filters.reduce((result, { name, value }) => {
					if (name === 'id') {
						const ids = value.split(',')?.map(i => parseInt(i, 10));
						Object.assign(result, { ids });
						return result;
					}
					Object.assign(result, { [name]: value });
					return result;
				}, {});

				await dispatch('fetchMessages', customFilters);
			},

			/** Скачать текущую отфильтрованную историю сообщений в CSV */
			downloadTableAsFile({ getters: { messages, allowedTableHeaders } }: { getters: IGetters }) {
				saveAsCsv<TGetterMessages>({
					content: messages,
					fields: allowedTableHeaders.map(i => i.value),
				});
			},
		},
	};
}

type TCustomFilters = {
	id?: string;
	contacts?: string[];
	orderId?: string;
	deliveryId?: string;
	partnerId?: string;
	event?: string;
	channel?: string;
	status?: string;
};

type TCustomFilter = {
	name: string;
	value: string;
};

export type TTableHeaders = {
	text: string;
	value: string;
}[];

interface IState {
	messages: TMesssages;
	errorMessage: string;
	isRequestInprogress: boolean;
	filterDateFrom: string;
	filterDateTo: string;
	allowedFields: string[];
	changelog: TMessageChangelog;
}

interface IGetters {
	messages: TGetterMessages;
	isRequestInprogress: boolean;
	filterDateFrom: string;
	filterDateTo: string;
	allowedTableHeaders: TTableHeaders;
}

export type TGetterMessages = MessageEntity[];
export type TGetterTableHeaders = TTableHeaders;
export type TGetterChangelog = TMessageChangelog;
export type TMutationSelectFields = (fields: string[]) => void;
export type TActionFetchMessages = () => Promise<void>;
export type TActionSelectDateFrom = (dateFrom: string) => Promise<void>;
export type TActionSelectDateTo = (dateTo: string) => Promise<void>;
export type TActionChangeCustomFilter = (filters: TCustomFilter[]) => void;
export type ActionFetchMessageChangelog = (messageId: number) => Promise<void>;
export type TActionDownloadTableAsFile = () => void;
