/* eslint-disable @typescript-eslint/no-explicit-any */
import Vue from 'vue';
import Vuex from 'vuex';
import nock from 'nock';
import apiConfig from '@/config/api';
import getMessagesStore from '@/modules/messages/store/messages-store';
import saveAsCsv from '@/utils/save-as-csv';
import MessageEntity, { TMessage } from '@/entities/message-entity';

Vue.config.productionTip = false;
Vue.config.devtools = false;
Vue.use(Vuex);

jest.mock('@/utils/save-as-csv');

const API_HOST = 'http://localhost/';
function createNewStore() {
	return new Vuex.Store<any>(getMessagesStore());
}

describe('MessagesStore', () => {
	describe('getters', () => {
		describe('changelog', () => {
			const tests = [
				{
					input: [],
					expected: [],
				},
				{
					input: [
						{
							id: 11111,
							createdAt: '2020-01-30T13:15:30+03:00',
							initiatedBy: 'owner',
							changes: {},
						},
					],
					expected: [
						{
							createdAt: '30.01.2020 13:15',
							statusCode: '',
							initiatedBy: 'owner',
						},
					],
				},
				{
					input: [
						{
							id: 11111,
							createdAt: '2020-01-30T13:15:30+03:00',
							initiatedBy: 'owner',
							changes: {
								old: { statusCode: 'crated' },
								new: { statusCode: 'sent' },
							},
						},
						{
							id: 11111,
							createdAt: '2020-12-30T13:15:30+03:00',
							initiatedBy: 'user',
							changes: {
								old: { statusCode: 'crated' },
								new: { statusCode: 'failed' },
							},
						},
					],
					expected: [
						{
							createdAt: '30.01.2020 13:15',
							statusCode: 'sent',
							initiatedBy: 'owner',
						},
						{
							createdAt: '30.12.2020 13:15',
							statusCode: 'failed',
							initiatedBy: 'user',
						},
					],
				},
				{
					input: [
						{
							id: 11111,
							createdAt: '2020-01-30T13:15:30+03:00',
							initiatedBy: 'owner',
							changes: {
								old: { statusCode: 'crated' },
								new: { statusCode: 'read' },
							},
						},
						{
							id: 11111,
							createdAt: '2020-12-30T13:15:30+03:00',
							initiatedBy: 'user',
							changes: {
								old: { statusCode: 'crated' },
								new: { statusCode: 'shown' },
							},
						},
					],
					expected: [
						{
							createdAt: '30.01.2020 13:15',
							statusCode: 'read',
							initiatedBy: 'owner',
						},
						{
							createdAt: '30.12.2020 13:15',
							statusCode: 'shown',
							initiatedBy: 'user',
						},
					],
				},
			];

			tests.forEach(({ input, expected }) => {
				it('Должен вернуть историю изменений статуса', () => {
					const store = createNewStore();
					store.commit('changelog', input);
					expect(store.getters.changelog).toEqual(expected);
				});
			});
		});

		describe('allowedTableHeaders', () => {
			const tests = [
				{
					input: [],
					expected: [],
				},
				{
					input: ['id', 'orderId'],
					expected: [
						{ text: 'Id сообщения', value: 'id' },
						{ text: 'Заказ', value: 'orderId' },
					],
				},
				{
					input: ['id', 'orderId', 'deliveryId', 'channel', 'status'],
					expected: [
						{ text: 'Id сообщения', value: 'id' },
						{ text: 'Заказ', value: 'orderId' },
						{ text: 'Доставка', value: 'deliveryId' },
						{ text: 'Канал', value: 'channel' },
						{ text: 'Статус', value: 'status' },
					],
				},
			];

			tests.forEach(({ input, expected }) => {
				it('Должен вернуть заголовки для таблицы', () => {
					const store = createNewStore();
					store.commit('allowedFields', input);
					expect(store.getters.allowedTableHeaders).toEqual(expected);
				});
			});
		});
	});
	describe('actions', () => {
		describe('fetchMessages', () => {
			const tests = [
				{
					input: {
						filterDateFrom: '2020-01-20T13:15:30+03:00',
						filterDateTo: '2021-02-25T13:09:28+03:00',
					},
					expected: [
						{ id: 11111, createdAt: '2020-01-30T13:15:30+03:00' },
						{ id: 22222, createdAt: '2020-11-30T13:15:30+03:00' },
						{ id: 33333, createdAt: '2020-06-10T13:15:30+03:00' },
					],
				},
				{
					input: {
						filterDateFrom: '2020-11-20T13:15:30+03:00',
						filterDateTo: '2021-12-25T13:09:28+03:00',
					},
					expected: [
						{ id: 44444, createdAt: '2020-11-30T13:15:30+03:00' },
						{ id: 22222, createdAt: '2020-12-30T13:15:30+03:00' },
						{ id: 55555, createdAt: '2020-12-10T13:15:30+03:00' },
					],
				},
				{
					input: {
						filterDateFrom: '2019-11-20T13:15:30+03:00',
						filterDateTo: '2022-12-25T13:09:28+03:00',
					},
					expected: [
						{ id: 66666, createdAt: '2020-11-30T13:15:30+03:00' },
						{ id: 88888, createdAt: '2020-12-30T13:15:30+03:00' },
						{ id: 99999, createdAt: '2020-12-10T13:15:30+03:00' },
					],
				},
				{
					input: {
						filterDateFrom: '2020-01-20T13:15:30+03:00',
						filterDateTo: undefined,
					},
					expected: [
						{ id: 11111, createdAt: '2020-01-30T13:15:30+03:00' },
						{ id: 22222, createdAt: '2020-11-30T13:15:30+03:00' },
						{ id: 33333, createdAt: '2020-06-10T13:15:30+03:00' },
					],
				},
				{
					input: {
						filterDateFrom: undefined,
						filterDateTo: '2022-12-25T13:09:28+03:00',
					},
					expected: [
						{ id: 66666, createdAt: '2020-11-30T13:15:30+03:00' },
						{ id: 88888, createdAt: '2020-12-30T13:15:30+03:00' },
						{ id: 99999, createdAt: '2020-12-10T13:15:30+03:00' },
					],
				},
				{
					input: {
						filterDateFrom: undefined,
						filterDateTo: undefined,
					},
					expected: [
						{ id: 66666, createdAt: '2020-11-30T13:15:30+03:00' },
						{ id: 88888, createdAt: '2020-12-30T13:15:30+03:00' },
						{ id: 99999, createdAt: '2020-12-10T13:15:30+03:00' },
					],
				},
			];

			tests.forEach(({ input, expected }) => {
				it(`Должен получить сообщения с ${input.filterDateFrom} по ${input.filterDateTo}`, async () => {
					nock(API_HOST)
						.post(`/${apiConfig.messagesGet}`, {
							data: {
								createdAfter: input.filterDateFrom,
								createdBefore: input.filterDateTo,
								limit: 300,
							},
						})
						.reply(200, {
							success: 1,
							data: {
								messages: expected,
							},
						});

					const store = createNewStore();
					store.commit('filterDateFrom', input.filterDateFrom);
					store.commit('filterDateTo', input.filterDateTo);

					const promise = store.dispatch('fetchMessages');
					expect(store.state.errorMessage).toBe('');
					expect(store.state.isRequestInprogress).toBe(true);
					await promise;

					expect(store.state.messages).toEqual(expected);
					expect(store.state.errorMessage).toBe('');
					expect(store.state.isRequestInprogress).toBe(false);
				});
			});

			it('Должен записать сообщение об ошибке', async () => {
				const [{ input }] = tests;
				nock(API_HOST)
					.post(`/${apiConfig.messagesGet}`, {
						data: {
							createdAfter: input.filterDateFrom,
							createdBefore: input.filterDateTo,
							limit: 300,
						},
					})
					.reply(500);

				const store = createNewStore();
				store.commit('filterDateFrom', input.filterDateFrom);
				store.commit('filterDateTo', input.filterDateTo);

				const promise = store.dispatch('fetchMessages');
				expect(store.state.isRequestInprogress).toBe(true);
				await promise.catch(() => {
					expect(store.state.errorMessage).toBe('Не удалось получить список сообщений');
				});

				expect(store.state.messages).toEqual([]);
				expect(store.state.isRequestInprogress).toBe(false);
			});
		});

		describe('changeCustomFilter', () => {
			const tests = [
				{
					input: {
						customFilters: [],
					},
					expected: [
						{ id: 11111, createdAt: '2020-01-30T13:15:30+03:00' },
						{ id: 22222, createdAt: '2020-11-30T13:15:30+03:00' },
						{ id: 33333, createdAt: '2020-06-10T13:15:30+03:00' },
					],
				},
				{
					input: {
						customFilters: [
							{
								text: 'id',
								value: '11111',
							},
							{
								text: 'deliveryId',
								value: '22222',
							},
							{
								text: 'partnerId',
								value: '33333',
							},
						],
					},
					expected: [
						{ id: 11111, createdAt: '2020-01-30T13:15:30+03:00', context: { deliveryId: '22222' } },
						{ id: 22222, createdAt: '2020-11-30T13:15:30+03:00', context: { partnerId: '33333' } },
						{ id: 33333, createdAt: '2020-06-10T13:15:30+03:00' },
					],
				},
				{
					input: {
						customFilters: [
							{
								text: 'id',
								value: '11111, 12121212, 13131313',
							},
							{
								text: 'deliveryId',
								value: '22222',
							},
							{
								text: 'partnerId',
								value: '33333',
							},
						],
					},
					expected: [
						{ id: 11111, createdAt: '2020-01-30T13:15:30+03:00', context: { deliveryId: '22222' } },
						{ id: 22222, createdAt: '2020-11-30T13:15:30+03:00', context: { partnerId: '33333' } },
						{ id: 33333, createdAt: '2020-06-10T13:15:30+03:00' },
						{ id: 12121212, createdAt: '2020-06-10T13:15:30+03:00' },
						{ id: 13131313, createdAt: '2020-06-10T13:15:30+03:00' },
					],
				},
				{
					input: {
						customFilters: [
							{
								text: 'id',
								value: '11111',
							},
							{
								text: 'contacts',
								value: 'dtqP42kzR8SC50Qz6lIXrm:APA91bGc1pAsuY',
							},
							{
								text: 'partnerId',
								value: '33333',
							},
						],
					},
					expected: [
						{ id: 11111, createdAt: '2020-01-30T13:15:30+03:00', context: { deliveryId: '22222' } },
						{ id: 22222, createdAt: '2020-11-30T13:15:30+03:00', context: { partnerId: '33333' } },
						{
							id: 33333,
							createdAt: '2020-06-10T13:15:30+03:00',
							recipient: {
								addresses: ['dtqP42kzR8SC50Qz6lIXrm:APA91bGc1pAsuY'],
							},
						},
					],
				},
			];

			tests.forEach(({ input, expected }) => {
				it('Должен получить сообщения с выбранными фильтрами', async () => {
					const filterDateFrom = '2020-01-20T13:15:30+03:00';
					const filterDateTo = '2021-02-25T13:09:28+03:00';

					nock(API_HOST)
						.post(`/${apiConfig.messagesGet}`, {
							data: {
								createdAfter: filterDateFrom,
								createdBefore: filterDateTo,
								limit: 300,
							},
						})
						.reply(200, {
							success: 1,
							data: {
								messages: expected,
							},
						});

					const store = createNewStore();
					store.commit('filterDateFrom', filterDateFrom);
					store.commit('filterDateTo', filterDateTo);

					const promise = store.dispatch('changeCustomFilter', input.customFilters);
					expect(store.state.errorMessage).toBe('');
					expect(store.state.isRequestInprogress).toBe(true);
					await promise;
					expect(store.state.messages).toEqual(expected);
					expect(store.state.isRequestInprogress).toBe(false);
				});
			});
		});

		describe('fetchMessageChangelog', () => {
			const tests = [
				{
					input: { messageId: 11111 },
					expected: [
						{
							id: 123123,
							messageId: 11111,
							createdAt: '2006-01-02T15:04:05+07:00',
							initiatedBy: 'initiator',
							changes: {
								old: { statusCode: 'pending' },
								new: { statusCode: 'sent' },
							},
						},
					],
				},
				{
					input: { messageId: 22222 },
					expected: [
						{
							id: 123123,
							messageId: 22222,
							createdAt: '2006-01-02T15:04:05+07:00',
							initiatedBy: 'initiator',
							changes: {},
						},
					],
				},
				{
					input: { messageId: 33333 },
					expected: [],
				},
				{
					input: { messageId: 44444 },
					expected: [
						{
							id: 123123,
							messageId: 44444,
							createdAt: '2006-12-02T15:04:05+07:00',
							initiatedBy: 'admin',
							changes: {},
						},
						{
							id: 3232323,
							messageId: 44444,
							createdAt: '2006-11-02T15:04:05+07:00',
							initiatedBy: 'initiator',
							changes: {},
						},
					],
				},
			];

			tests.forEach(({ input, expected }) => {
				it(`Должен получить историю изменений сообшения id:${input.messageId}`, async () => {
					nock(API_HOST)
						.post(`/${apiConfig.changelogGet}`, {
							data: {
								messageId: input.messageId,
								limit: 100,
							},
						})
						.reply(200, {
							success: 1,
							data: { log: expected },
						});

					const store = createNewStore();

					const promise = store.dispatch('fetchMessageChangelog', input.messageId);
					expect(store.state.errorMessage).toBe('');
					expect(store.state.isRequestInprogress).toBe(true);
					await promise;
					expect(store.state.changelog).toEqual(expected);
					expect(store.state.errorMessage).toBe('');
					expect(store.state.isRequestInprogress).toBe(false);
				});
			});

			it('Должен записать сообщение об ошибке', async () => {
				const [{ input }] = tests;
				nock(API_HOST)
					.post(`/${apiConfig.changelogGet}`, {
						data: {
							messageId: input.messageId,
							limit: 100,
						},
					})
					.reply(500);

				const store = createNewStore();

				const promise = store.dispatch('fetchMessageChangelog', input.messageId);
				expect(store.state.errorMessage).toBe('');
				expect(store.state.isRequestInprogress).toBe(true);
				await promise;
				expect(store.state.errorMessage).toBe('Не удалось получить changelog для сообщения 11111');
				expect(store.state.isRequestInprogress).toBe(false);
				expect(store.state.changelog).toEqual([]);
			});
		});

		describe('downloadTableAsFile', () => {
			const expectedFields = [
				'creationDateFormatted',
				'id',
				'contacts',
				'orderId',
				'deliveryId',
				'event',
				'channel',
				'partnerId',
				'status',
			];
			const tests = [
				{
					input: {
						messages: [],
					},
					expected: {
						content: [],
						fields: expectedFields,
					},
				},
				{
					input: {
						messages: [
							{ id: 11111, createdAt: '2020-01-30T13:15:30+03:00' },
							{ id: 22222, createdAt: '2020-11-30T13:15:30+03:00' },
							{ id: 33333, createdAt: '2020-06-10T13:15:30+03:00' },
						],
					},
					expected: {
						content: [
							{ id: 11111, createdAt: '2020-01-30T13:15:30+03:00' },
							{ id: 22222, createdAt: '2020-11-30T13:15:30+03:00' },
							{ id: 33333, createdAt: '2020-06-10T13:15:30+03:00' },
						].map(i => new MessageEntity(i as TMessage)),
						fields: expectedFields,
					},
				},
				{
					input: {
						messages: [
							{ id: 11111, createdAt: '2020-01-30T13:15:30+03:00', context: { deliveryId: '22222' } },
							{ id: 22222, createdAt: '2020-11-30T13:15:30+03:00', context: { partnerId: '33333' } },
							{ id: 33333, createdAt: '2020-06-10T13:15:30+03:00' },
							{ id: 12121212, createdAt: '2020-06-10T13:15:30+03:00' },
							{ id: 13131313, createdAt: '2020-06-10T13:15:30+03:00' },
						],
					},
					expected: {
						content: [
							{ id: 11111, createdAt: '2020-01-30T13:15:30+03:00', context: { deliveryId: '22222' } },
							{ id: 22222, createdAt: '2020-11-30T13:15:30+03:00', context: { partnerId: '33333' } },
							{ id: 33333, createdAt: '2020-06-10T13:15:30+03:00' },
							{ id: 12121212, createdAt: '2020-06-10T13:15:30+03:00' },
							{ id: 13131313, createdAt: '2020-06-10T13:15:30+03:00' },
						].map(i => new MessageEntity(i as TMessage)),
						fields: expectedFields,
					},
				},
			];

			tests.forEach(({ input, expected }) => {
				it('Должен инициировать скачивание файла с верными параметрами', () => {
					const store = createNewStore();
					store.commit('messages', input.messages);
					store.dispatch('downloadTableAsFile');

					expect(saveAsCsv).toBeCalledWith(expected);
				});
			});
		});
	});
});
